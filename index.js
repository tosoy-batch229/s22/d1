// Array Methods
// JS has built-in functions and methods for arrays.

// [ Mutator Methods ]
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
// Adds new elements to the end of an array, and returns the new length.
// SYNTAX : arrayName.push();
console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated Array from push method.");
console.log(fruits);

// Adding multiple elements to an array.
fruits.push("Avocado", "Guava");
console.log("Mutated Array from push method.");
console.log(fruits);

// pop()
// Removes the last element of an array, and returns that element.
let removedFruit = fruits.pop();
console.log("Mutated Array from pop method.");
console.log(removedFruit);
console.log(fruits);

// unshift()
// Adds new elements to the beginning of an array, and returns the new length.
fruits.unshift("Lime", "Banana");
console.log("Mutated Array from unshift method.");
console.log(fruits);

// shift()
// Removes the first element of an array, and returns that element.
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated Array from shift method.");
console.log(fruits);

// splice()
// Adds/Removes elements from a specified index in an array.
// SYNTAX : arrayName.splice(start, deleteCount, elementsToBeAdded);
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated Array from splice method.");
console.log(fruits);

// sort()
// Sorts the elements as strings in alphabetical and ascending order.
fruits.sort();
console.log("Mutated Array from sort method.");
console.log(fruits);

// reverse()
// Reverses the order of the elements in an array.
fruits.reverse();
console.log("Mutated Array from reverse method.");
console.log(fruits);

// [ NON-MUTATOR METHODS ]
// Unlike the mutator methods, non-mutator methods cannot modify the array.
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
// Returns the first index (position) of a specified value, returns -1 if the value is not found.
// Starts at a specified index and searches from left to right. Negative start values counts from the last element (but still searches from left to right).
// SYNTAX : arrayName.indexOf(searchValue, fromIndex);
let firstIndex = countries.indexOf("PH");
console.log("Result of index method : " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of index method : " + invalidCountry);

// lastIndexOf()
// Returns the last index (position) of a specified value, returns -1 if the value is not found.
// Starts at a specified index and searches from right to left, Negative start values counts from the last element (but still searches from right to left).
// SYNTAX : arrayName.lastIndexOf(searchValue, fromIndex);
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastindex method : " + lastIndex);

// Getting the index number starting from specified index
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastindex method : " + lastIndexStart);

// slice()
// Returns selected elements in an array, as a new array.
// It selects from a given start, up to a (not inclusive) given end.
// SYNTAX : arrayName.slice(start, end);

let sliceArrayA = countries.slice(2);
console.log("Result from slice method");
console.log(sliceArrayA);

let sliceArrayB = countries.slice(2, 4);
console.log("Result from slice method");
console.log(sliceArrayB);

// slicing off elements starting from the last elements of an array.
let sliceArrayC = countries.slice(-3);
console.log("Result from slice method");
console.log(sliceArrayC);

// toString()
// Returns a string with array values separated by commas.
let stringArray = countries.toString();
console.log("Result from toString method");
console.log(stringArray);

// concat()
// Concatenates (joins) two or more arrays, returns a new array, containing the joined arrays.
// SYNTAX : arrayA.concat(arrayB);
let taskArray1 = ["Drink HTML", "Eat JS"], taskArray2 = ["Inhale CSS", "Breath SASS"], taskArray3 = ["Get Git", "Be Node"];
let task = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(task);

// Combining multiple arrays.
console.log("Result from concat method");
let allTask = taskArray1.concat(taskArray2, taskArray3);
console.log(allTask);

// Combining Arrays with Elements
console.log("Result from concat method");
let combinedTask = taskArray1.concat("Smell Express", "Throw React");
console.log(combinedTask);

// join() 
// Returns an array as a string separated by specified separator string.
// SYNTAX : arrayName.join('seperatorsString');
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// forEach()
// Calls a function for each element in an array, and it is not executed for empty elements.
// SYNTAX : arrayName.forEach(function(individualElement) {statement});
allTask.forEach(function(task) {
    console.log(task);
});

// Using forEach with conditional statement
let filteredTasks = [];
allTask.forEach(function(task) {
    if (task.length >= 10) {
        filteredTasks.push(task);
    }
});

console.log("Result of filtered task");
console.log(filteredTasks);

// map()
// This is useful for performing tasks where mutating/changing the elements are required.
// SYNTAX : let/const resultArray = arrayName.map(function(individualElement) {statement} );
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number) {
    return number * number;
});
console.log("Original Array: ");
console.log(numbers); // Original is not affected by map()
console.log("Result from map method");
console.log(numberMap); // A new array is returned and stored in a variable.

// map() vs forEach()
let numberForEach = numbers.forEach(function(number) {
    return number * number;
});
console.log(numberForEach); // undefined
// forEach(), loops over all items in the array as maps(), but forEach() does not return new array.

// every()
// checks if all elements in an array meet the given condition
// returns true if the function returns true for all elements, returns false if the function returns false for one element.
// SYNTAX : let/const resultArray = arrayName.every(function(individualElement) {statement} );
let allValid = numbers.every(function(number) {
    return (number < 3);
});
console.log("Results from every method");
console.log(allValid);

// some() 
// checks if any array elements pass a test (provided as a callback function). executes the callback function once for each array element. 
// returns true (and stops) if the function returns true for one of the array elements, returns false if the function returns false for all of the array elements.
// SYNTAX : let/const resultArray = arrayName.some(function(individualElement) {statement} );
let someValid = numbers.some(function(number) {
    return (number < 2);
});
console.log("Results from some method");
console.log(someValid);

// filter()
// Returns new array that contains elements which meet the given condition.
// Return an empty array if no elements were found.
// SYNTAX : let/const resultArray = arrayName.filter(function(individualElement) {statement});
let filterValid = numbers.filter(function(number){
    return (number < 3);
})
console.log("Results from filter method");
console.log(filterValid);

// includes()
// Checks if the argument passed can be found in the array.
// Returns true if an array contains a specified value, returns false if the value is not found.
// Syntax : arrayName.includes(argumentToFind);
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// combined filter and include method
let filteredProduct = products.filter(function(product) {
    return product.toLowerCase().includes("a");
})
console.log(filteredProduct);

// reduce()
// Evaluates elements from left to right and returns/reduces the array into a single value.
// SYNTAX : let/const resultArray = arrayName.reduce(function(accumalator, currentValue) {statement});
let iteration = 0;
let reducedArray = numbers.reduce(function(x, y) {
    console.warn("Current Iteration: " + ++iteration);
    console.log("accumulator: " + x);
    console.log("currentValue: " + y);

    // The operation to reduce the array into single value.
    return x + y;
})
console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];
let reduceJoin = list.reduce(function(x, y) {
    return x + " " + y;
});
console.log("Result of reduce method");
console.log(list);
